import * as express from 'express';

import * as wordController from './controller/wordController';

const app  = express();

app.set("port",3000);

app.get('/put',wordController.putWord);

app.get('/get',wordController.displayWord);

app.listen(app.get("port"),()=>{
    console.log('App is running');
})