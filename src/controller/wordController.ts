import * as express from 'express';
import Words from '../model/words'

//Dummy Entry
export let putWord = (req:express.Request,res:express.Response) => {
    let word  = new Words({
        hint:'Mans best friend',
        word:'DOG'
    });
    word.save().then(result=>{
        console.log('RESULT : ',result);
    }).catch(err=>{
        console.log(err);
    });
}

let R: number;
export let displayWord = (req:express.Request,res:express.Response) => {
    //Counts no of documents in  collection
    Words.count({})
    .then(count=>{
        console.log('COUNTS : ',count);
        R=Math.floor(Math.random()*count);
    
      }).catch(err=>{console.log(err)});

    //fetches a random document from collection
    Words.find().limit(1).skip(R)
    .then(words =>{
        res.json({words});       
    })
    .catch(err=>{
        console.log(err);
    })

}