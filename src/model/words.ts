import * as mongoose from 'mongoose';

const uri :string ='mongodb+srv://Varun:8wct9xtkAzNwZOJ4@cluster0.whpqx.mongodb.net/RandomWords?retryWrites=true&w=majority';

mongoose.connect(uri,(err)=>{
    if(err)
        console.log(err);
    else
        console.log('Connected to mongoose');
});

export const WordSchema = new mongoose.Schema({
    "hint":{
        type:String,
        required:true
    },
    "word":{
        type:String,
        required:true
    }
});

const Words = mongoose.model('Words',WordSchema);

export default Words;