"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.displayWord = exports.putWord = void 0;
const words_1 = require("../model/words");
exports.putWord = (req, res) => {
    let word = new words_1.default({
        hint: 'Selfish Pet',
        word: 'CAT'
    });
    word.save().then(result => {
        console.log('RESULT : ', result);
    }).catch(err => {
        console.log(err);
    });
};
let R;
exports.displayWord = (req, res) => {
    //Counts no of documents in  collection
    words_1.default.count({})
        .then(count => {
        console.log('COUNTS : ', count);
        R = Math.floor(Math.random() * count);
    }).catch(err => { console.log(err); });
    //fetches a random document from collection
    words_1.default.find().limit(1).skip(R)
        .then(words => {
        res.json({ words });
    })
        .catch(err => {
        console.log(err);
    });
};
//# sourceMappingURL=wordController.js.map