"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WordSchema = void 0;
const mongoose = require("mongoose");
const uri = 'mongodb+srv://Varun:8wct9xtkAzNwZOJ4@cluster0.whpqx.mongodb.net/RandomWords?retryWrites=true&w=majority';
mongoose.connect(uri, (err) => {
    if (err)
        console.log(err);
    else
        console.log('Connected to mongoose');
});
exports.WordSchema = new mongoose.Schema({
    "hint": {
        type: String,
        required: true
    },
    "word": {
        type: String,
        required: true
    }
});
const Words = mongoose.model('Words', exports.WordSchema);
exports.default = Words;
//# sourceMappingURL=words.js.map