"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const wordController = require("./controller/wordController");
const app = express();
app.set("port", 3000);
app.get('/put', wordController.putWord);
app.get('/get', wordController.displayWord);
app.listen(app.get("port"), () => {
    console.log('App is running');
});
//# sourceMappingURL=app.js.map